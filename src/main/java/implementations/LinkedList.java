package implementations;

import exceptions.EmptyListException;
import interfaces.List;

/**
 * A simple implementation of LinkedList.
 * 
 * @author Ole Granberg
 * @param <T> - The datatype to be stored.
 */
public class LinkedList<T> implements List<T> 
{
    private ListNode<T> head;

    public LinkedList() 
    {
        this.head = null;
    }

    @Override
    public void clear() 
    {
        this.head = null;
    }

    @Override
    public boolean isEmpty() 
    {
        return head == null;
    }

    @Override
    public int numbrOfElemensInList() 
    {
        int size = 0;
        
        ListNode<T> currentNode = head;
        
        while(currentNode != null)
        {
            size++;
            currentNode = currentNode.next;
        }
        
        return size;
    }

    @Override
    public void insertFirst(T data) 
    {
        if (isEmpty()) 
        {
            this.head = new ListNode<>(null, data);
        }
        else
        {
            this.head = new ListNode<>(this.head, data);
        }
    }

    @Override
    public T getFirst() 
    {
        if(!isEmpty())
        {
            return head.data;
        }
        else
            throw new EmptyListException("Get first is not allowed on a emptt list.");
    }

    @Override
    public T removeFirst() 
    {        
        if (!isEmpty()) 
        {
            T removedData = getFirst();

            this.head = this.head.next;
            
            return removedData;
        }
        else
            throw new EmptyListException("Can't remove data from a an empty list");
    }

    @Override
    public void insertLast(T data) 
    {
        if (isEmpty()) 
        {
            insertFirst(data);
        } 
        else 
        {
            ListNode<T> lastNode = findLastNode();
            lastNode.next = new ListNode<>(null, data);
        }
    }

    @Override
    public T getLast() 
    {
        if(!isEmpty())
        {
            ListNode<T> lastNode = findLastNode();

            return lastNode.data;
        }
        else
            throw new EmptyListException("Can't remove data from a an empty list");
        
    }

    @Override
    public T removeLast() 
    {
        ListNode<T> lastNode = findLastNode();
        
        T removedData = lastNode.next.data;
        
        lastNode.next = null;
        
        return removedData;
    }
    
    private ListNode<T> findLastNode()
    {
        ListNode<T> currentNode = head;
         
        while(currentNode.next != null)
        {
            currentNode = currentNode.next;
        }
        
        return currentNode;
    }

    @Override
    public void printContent() 
    {
        if(this.head == null)
        {
            System.out.println("[]");
        }
        else 
        {
            ListNode<T> currentNode = head;

            while (currentNode.next != null) 
            {
                System.out.println(currentNode.data.toString());
                currentNode = currentNode.next;
            }
        }
    }

    
}

class ListNode<T>
{
    public ListNode<T> next;
    public T data;

    public ListNode(ListNode<T> next, T data) 
    {
        this.next = next;
        this.data = data;
    }
}
