package interfaces;

/**
 * A interface for the abstract datatype List.
 * 
 * This interface describes what operations are expected of a list.
 *
 * @author Ole Granberg
 * @param <T> - The datatype to be stored.
 */
public interface List<T> 
{
    /**
     * Clear the list.
     */
    public void clear();
    
    /**
     * Check if the list is empty.
     * 
     * @return True if empty, otherwise false.
     */
    public boolean isEmpty();
    
    /**
     * Get the total number of elements in the list.
     * 
     * @return The number of elements in the list.
     */
    public int numbrOfElemensInList();
    
    /**
     * Insert a element first in the list.
     * @param t - the element to be stored.
     */
    public void insertFirst(T t);
    
    /**
     * Fetch the first element from the list, without removing it.
     * 
     * @return T - The fetched element.
     */
    public T getFirst();
    
    /**
     * Remove the first element in the list.
     * 
     * @return T - The removed element.
     */
    public T removeFirst();
    
    /**
     * Insert a element last in the list.
     * @param t - the element to be stored.
     */
    public void insertLast(T t);
    
    /**
     * Fetch the last element from the list, without removing it.
     * 
     * @return T - The fetched element.
     */
    public T getLast();
    
    /**
     * Remove the last element in the list.
     * 
     * @return T - The removed element.
     */
    public T removeLast();
    
    /**
     * Print the content of the list.
     * 
     */
    public void printContent();
}
