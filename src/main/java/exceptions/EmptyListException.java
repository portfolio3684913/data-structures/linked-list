/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Exception.java to edit this template
 */
package exceptions;

/**
 *
 * @author rcfly
 */
public class EmptyListException extends RuntimeException {

    /**
     * Creates a new instance of <code>EmptyListException</code> without detail
     * message.
     */
    public EmptyListException() {
    }

    /**
     * Constructs an instance of <code>EmptyListException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public EmptyListException(String msg) {
        super(msg);
    }
}
