/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package implementations;

import exceptions.EmptyListException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test of the linked list implementation.
 * 
 * @author Ole Granberg
 */
public class LinkedListTest
{
    private LinkedList<Integer> orderedList = new LinkedList<Integer>();
    private LinkedList<Integer> emptyList = new LinkedList<Integer>();
    private final int[] orderedNumbers = {1,2,3,4,5,6};
    
    public LinkedListTest() 
    {
    }
    
    @BeforeAll
    public static void setUpClass() 
    {
    }
    
    @AfterAll
    public static void tearDownClass() 
    {
    }
    
    @BeforeEach
    public void setUp() 
    {
        initialazeList(this.orderedList);
        this.emptyList.clear();
    }
    
    @AfterEach
    public void tearDown() 
    {
        orderedList.clear();
        this.emptyList.clear();
    }
    
    public void initialazeList(LinkedList<Integer> list)
    {
        for (int testNumbr : orderedNumbers) 
        {
            orderedList.insertLast(testNumbr);
        }
    }

    /**
     * Test of clear method on a not-empty list, of class LinkedList.
     */
    @Test
    public void testClearOnNonEmptyList() 
    {
        System.out.println("Test of clear on a not-empty list.");
        
        //Double check manualy:
        orderedList.printContent();
        
        assertFalse(orderedList.isEmpty());
        
        orderedList.clear();
        
        assertTrue(orderedList.isEmpty());
        
        //Double check manualy:
        orderedList.printContent();
    }   
    
    /**
     * Test of clear method on an empty list, of class LinkedList.
     */
    @Test
    public void testClearOnEmptyList() 
    {
        System.out.println("Test of clear on an empty list.");
        
        assertTrue(emptyList.isEmpty());
        
        orderedList.clear();
        
        assertTrue(orderedList.isEmpty());
        
        //Double check manualy:
        orderedList.printContent();
    }
    
    /**
     * Test of IsEmpty method on an non-empty list, of class LinkedList. 
     */
    @Test
    public void testIsEmptyOnNonEmptyList() 
    {
        System.out.println("Test of isEmpty on an non-empty list.");
        
        assertFalse(orderedList.isEmpty());
        
        //Double check manualy:
        orderedList.printContent();
    }
    
    /**
     * Test of IsEmpty method on an empty list, of class LinkedList. 
     */
    @Test
    public void testIsEmptyOnEmptyList() 
    {
        System.out.println("Test of isEmpty on an empty list.");
        
        assertTrue(emptyList.isEmpty());
        
        //Double check manualy:
        emptyList.printContent();
    }
    
    /**
     * Test of numberOfElements method on an non-empty list, of class LinkedList. 
     */
    @Test
    public void testNbrOfElementsOnNonEmptyList() 
    {
        System.out.println("Test of numberOfElements on an non-empty list.");
        
        assertEquals(6, orderedList.numbrOfElemensInList());
    }
    
    /**
     * Test of numberOfElements method on an empty list, of class LinkedList. 
     */
    @Test
    public void testNbrOfElementsOnEmptyList() 
    {
        System.out.println("Test of numberOfElements on an empty list.");
        
        assertEquals(0, emptyList.numbrOfElemensInList());
    }
    
    /**
     * Test of insertFirst method on an non-empty list, of class LinkedList. 
     */
    @Test
    public void testInsertFirstOnNonEmptyList() 
    {
        System.out.println("Test of insertFirst on an non-empty list.");
        
        assertEquals(6, orderedList.numbrOfElemensInList());
        
        orderedList.insertFirst(2);
        
        //Double check manualy:
        emptyList.printContent();
        
        assertEquals(7, orderedList.numbrOfElemensInList());
    }
    
    /**
     * Test of insertFirst method on an empty list, of class LinkedList. 
     */
    @Test
    public void testInsertFirstOnEmptyList() 
    {
        System.out.println("Test of insertFirst on an empty list.");
        
        assertEquals(0, emptyList.numbrOfElemensInList());
        
        emptyList.insertFirst(2);
        
        //Double check manualy:
        emptyList.printContent();
        
        assertEquals(1, emptyList.numbrOfElemensInList());
    }
    
    /**
     * Test of getFirst method on an non-empty list, of class LinkedList. 
     */
    @Test
    public void testGetFirstOnNonEmptyList() 
    {
        System.out.println("Test of getFirst on an non-empty list.");
        
        assertEquals(1, orderedList.getFirst());
        
        //Double check manualy:
        emptyList.printContent();
    }
    
    /**
     * Test of getFirst method on an non-empty list, of class LinkedList. 
     */
    @Test
    public void testGetFirstOnEmptyList() 
    {
        System.out.println("Test of getFirst on an empty list.");
        
        assertThrows(EmptyListException.class,() -> {emptyList.getFirst();});
    }
    
    /**
     * Test of removeFirst method on an non-empty list, of class LinkedList. 
     */
    @Test
    public void testRemoveFirstOnNonEmptyList() 
    {
        System.out.println("Test of removeFirst on an non-empty list.");
        
        assertEquals(6, orderedList.numbrOfElemensInList());
        
        assertEquals(1, orderedList.removeFirst());
        
        assertEquals(5, orderedList.numbrOfElemensInList());
    }
    
    /**
     * Test of removeFirst method on an empty list, of class LinkedList. 
     */
    @Test
    public void testRemoveFirstOnEmptyList() 
    {
        System.out.println("Test of removeFirst on an empty list.");
        
        assertThrows(EmptyListException.class,() -> {emptyList.removeFirst();});
    }
    
    /**
     * Test of insertLast method on an non-empty list, of class LinkedList. 
     */
    @Test
    public void testInsertLastOnNonEmptyList() 
    {
        System.out.println("Test of insertLast on an non-empty list.");
        
        assertEquals(6, orderedList.numbrOfElemensInList());
        
        assertEquals(6, orderedList.getLast());
        
        orderedList.insertLast(2);
        
        assertEquals(2, orderedList.getLast());
    }
    
    /**
     * Test of insertLast method on an empty list, of class LinkedList. 
     */
    @Test
    public void testInsertLastOnEmptyList() 
    {
        System.out.println("Test of insertLast on an empty list.");
        
        assertEquals(0, emptyList.numbrOfElemensInList());
        
        assertThrows(EmptyListException.class,() -> {emptyList.getLast();});
        
        emptyList.insertLast(2);
        
        assertEquals(2, emptyList.getLast());
    }
    
}
